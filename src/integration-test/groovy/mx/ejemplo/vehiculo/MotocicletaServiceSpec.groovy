package mx.ejemplo.vehiculo

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class MotocicletaServiceSpec extends Specification {

    MotocicletaService motocicletaService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Motocicleta(...).save(flush: true, failOnError: true)
        //new Motocicleta(...).save(flush: true, failOnError: true)
        //Motocicleta motocicleta = new Motocicleta(...).save(flush: true, failOnError: true)
        //new Motocicleta(...).save(flush: true, failOnError: true)
        //new Motocicleta(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //motocicleta.id
    }

    void "test get"() {
        setupData()

        expect:
        motocicletaService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Motocicleta> motocicletaList = motocicletaService.list(max: 2, offset: 2)

        then:
        motocicletaList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        motocicletaService.count() == 5
    }

    void "test delete"() {
        Long motocicletaId = setupData()

        expect:
        motocicletaService.count() == 5

        when:
        motocicletaService.delete(motocicletaId)
        sessionFactory.currentSession.flush()

        then:
        motocicletaService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Motocicleta motocicleta = new Motocicleta()
        motocicletaService.save(motocicleta)

        then:
        motocicleta.id != null
    }
}
