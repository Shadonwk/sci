package mx.ejemplo.vehiculo

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class SedanServiceSpec extends Specification {

    SedanService sedanService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Sedan(...).save(flush: true, failOnError: true)
        //new Sedan(...).save(flush: true, failOnError: true)
        //Sedan sedan = new Sedan(...).save(flush: true, failOnError: true)
        //new Sedan(...).save(flush: true, failOnError: true)
        //new Sedan(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //sedan.id
    }

    void "test get"() {
        setupData()

        expect:
        sedanService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Sedan> sedanList = sedanService.list(max: 2, offset: 2)

        then:
        sedanList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        sedanService.count() == 5
    }

    void "test delete"() {
        Long sedanId = setupData()

        expect:
        sedanService.count() == 5

        when:
        sedanService.delete(sedanId)
        sessionFactory.currentSession.flush()

        then:
        sedanService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Sedan sedan = new Sedan()
        sedanService.save(sedan)

        then:
        sedan.id != null
    }
}
