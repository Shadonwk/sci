package mx.ejemplo.vehiculo

import grails.gorm.services.Service

@Service(Sedan)
interface SedanService {

    Sedan get(Serializable id)

    List<Sedan> list(Map args)

    Long count()

    void delete(Serializable id)

    Sedan save(Sedan sedan)

}