package mx.ejemplo.vehiculo

import grails.gorm.services.Service

@Service(Motocicleta)
interface MotocicletaService {

    Motocicleta get(Serializable id)

    List<Motocicleta> list(Map args)

    Long count()

    void delete(Serializable id)

    Motocicleta save(Motocicleta motocicleta)

}