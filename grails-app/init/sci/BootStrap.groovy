package sci

import grails.util.Environment
import groovy.sql.Sql

class BootStrap {

    def dataSource
    def grailsApplication


    def init = { servletContext ->
        loadSqlData('catalogos')
    }

    /**Se encarga de realizar la carga de un archivo sql a la base de datos**/
    def loadSqlData(archivo) {
        try {
            def db = new Sql(dataSource)
            def is = grailsApplication.mainContext.getResource("classpath:BootstrapData/${archivo}.sql").inputStream
            log.info "::::::::: Carga ${archivo}... 0% :::::::::"
            println "::::::::: Carga ${archivo}... 0% :::::::::"
            is.eachLine { line ->
                if (Environment.current == Environment.DEVELOPMENT) {
                    if ((line.contains('set') || line.contains('SET')) && Environment.current == Environment.DEVELOPMENT) {
                        log.trace 'SET omitido'
                        println 'SET omitido'

                    } else if (line.trim().empty) {
                        log.trace 'línea en blanco omitida'
                        println 'línea en blanco omitida'
                    } else {
                        log.info line
                        println line
                        db.executeUpdate(line)
                    }
                }
                if (Environment.current == Environment.PRODUCTION) {
                    if (line.trim().empty) {
                        log.trace 'línea en blanco omitida'
                        println 'línea en blanco omitida'
                    } else {
                        log.info line
                        println line
                        db.executeUpdate(line)
                    }
                }

            }
        } catch (ex) {
            log.warn ex
        }
    }


    def destroy = {
    }
}
