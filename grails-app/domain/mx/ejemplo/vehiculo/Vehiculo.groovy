package mx.ejemplo.vehiculo

import mx.ejemplo.seguridad.Usuario

class Vehiculo {

    String marca
    String modelo
    String color
    String placa
    int numeroLlantas
    TipoVehiculo tipoVehiculo

    //propiedades de control de grails
    Date dateCreated
    Date lastUpdated
    Usuario usuarioModifica


    //se agrega para crear una tabla por cada objeto que herede esta clase
    static mapping = {
        tablePerHierarchy false
    }

    static constraints = {
        marca nullable: false
        modelo nullable: false
        color nullable: false
        tipoVehiculo display: false
        usuarioModifica display:false
    }
}
