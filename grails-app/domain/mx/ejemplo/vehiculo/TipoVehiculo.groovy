package mx.ejemplo.vehiculo

enum TipoVehiculo {

    SEDAN (1, "SEDAN" ),
    MOTOCICLETA (2, "MOTOCICLETA")

    final int id
    final String descripcion

    private TipoVehiculo(int id, String descripcion) {
        this.id = id
        this.descripcion = descripcion
    }

    @Override
    String toString() {
        return descripcion
    }

    static TipoVehiculo get(int id) {
        switch (id){
            case 1: return SEDAN
            case 2: return MOTOCICLETA
            default:
                throw new IllegalArgumentException("Dato incorrecto: " + String.valueOf(id))
        }
    }
}
