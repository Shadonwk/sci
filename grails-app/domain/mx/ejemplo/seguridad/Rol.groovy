package mx.ejemplo.seguridad


class Rol implements Serializable {

    private static final long serialVersionUID = 1

    String authority
    Date dateCreated
    Date lastUpdated
    Usuario usuarioModifica


    Rol(String authority) {
        this()
        this.authority = authority
    }

    static mapping = {
        cache true
    }

    static constraints = {
        dateCreated display: false
        lastUpdated display: false
        usuarioModifica display: false
        authority blank: false, unique: true
    }

    @Override
    String toString() {
        return authority
    }
}
