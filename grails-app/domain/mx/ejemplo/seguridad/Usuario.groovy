package mx.ejemplo.seguridad

import grails.databinding.BindingFormat

class Usuario {


    String username
    String password

    String nombre
    String aPaterno
    String aMaterno

    //propiedades de control de grails
    Date dateCreated
    Date lastUpdated
    Usuario usuarioModifica

    //requeridas por plugin de spring security
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired


    static mapping = {

    }

    Set<Rol> getAuthorities() {
        (UsuarioRol.findAllByUsuario(this) as List<UsuarioRol>)*.rol as Set<Rol>
    }

    static constraints = {

        dateCreated display: false
        lastUpdated display: false
        usuarioModifica  nullable: true, display: false
        password nullable: false, blank: false, password: true
        username nullable: false, blank: false, unique: true, maxSize: 100
        nombre nullable: false, blank: false, maxSize: 50
        aPaterno nullable: false, blank: false, maxSize: 50
        aMaterno nullable: false, blank: false, maxSize: 50

    }

    @Override
    String toString() {
        return "$nombre $aPaterno $aMaterno"
    }
}
