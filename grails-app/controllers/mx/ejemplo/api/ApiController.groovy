package mx.ejemplo.api

import grails.converters.JSON
import mx.ejemplo.vehiculo.Vehiculo
import mx.ejemplo.vehiculo.Motocicleta
import mx.ejemplo.vehiculo.Sedan



class ApiController {

    def vehiculoService
    def springSecurityService

    def index() { }

    //muestra la lista de vehículos almacenados
    def getVehiculos(){

          def vehiculos = Vehiculo.list()
          if (vehiculos) {
              response.status = 200
              render(vehiculos as JSON)
          } else {
              response.status = 204
          }

      }


    //se encarga de almacenar un vehículo dependiendo los datos enviados
    //puede ser SEDAN o MOTOCICLETA
    def save(){
            println("Se invoca saveVehiculo")

            try {
                //se parsean los datos y se crea el objeto a almacenar
                def final request2 = request.JSON
                log.debug(request2 as JSON)
                def vehiculo
                if(request2.tipoVehiculo.equals("SEDAN")){
                    vehiculo = new Sedan(request2)
                }else{
                    vehiculo = new Motocicleta(request2)
                }

                vehiculo.usuarioModifica = springSecurityService.currentUser

                if(vehiculo.validate())
                    vehiculo.save()
                else{
                    response.status = 404
                    render vehiculo.errors as JSON
                }
            }catch(JsonParseException){
                response.status = 204
                render([estatus: 'ERROR', descripcion: 'El JSON no se puede PARSEAR.'] as JSON)
                return
            }
        }

}
