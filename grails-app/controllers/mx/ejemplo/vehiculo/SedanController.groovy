package mx.ejemplo.vehiculo

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class SedanController {

    SedanService sedanService
    def springSecurityService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond sedanService.list(params), model:[sedanCount: sedanService.count()]
    }

    def show(Long id) {
        respond sedanService.get(id)
    }

    def create() {
        params.tipoVehiculo = TipoVehiculo.get(1)
        respond new Sedan(params)
    }

    def save(Sedan sedan) {
        if (sedan == null) {
            notFound()
            return
        }

        try {
            sedan.tipoVehiculo = TipoVehiculo.get(1)
            sedan.usuarioModifica = springSecurityService.currentUser
            sedanService.save(sedan)
        } catch (ValidationException e) {
            respond sedan.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'sedan.label', default: 'Sedan'), sedan.id])
                redirect sedan
            }
            '*' { respond sedan, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond sedanService.get(id)
    }

    def update(Sedan sedan) {
        if (sedan == null) {
            notFound()
            return
        }

        try {
            sedanService.save(sedan)
        } catch (ValidationException e) {
            respond sedan.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'sedan.label', default: 'Sedan'), sedan.id])
                redirect sedan
            }
            '*'{ respond sedan, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        sedanService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'sedan.label', default: 'Sedan'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'sedan.label', default: 'Sedan'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
