package mx.ejemplo.vehiculo

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class MotocicletaController {

    MotocicletaService motocicletaService
    def springSecurityService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond motocicletaService.list(params), model:[motocicletaCount: motocicletaService.count()]
    }

    def show(Long id) {
        respond motocicletaService.get(id)
    }

    def create() {
        params.tipoVehiculo = TipoVehiculo.get(2)
        respond new Motocicleta(params)
    }

    def save(Motocicleta motocicleta) {
        if (motocicleta == null) {
            notFound()
            return
        }

        try {
            motocicleta.tipoVehiculo = TipoVehiculo.get(2)
            motocicleta.usuarioModifica = springSecurityService.currentUser
            motocicletaService.save(motocicleta)
        } catch (ValidationException e) {
            respond motocicleta.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'motocicleta.label', default: 'Motocicleta'), motocicleta.id])
                redirect motocicleta
            }
            '*' { respond motocicleta, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond motocicletaService.get(id)
    }

    def update(Motocicleta motocicleta) {
        if (motocicleta == null) {
            notFound()
            return
        }

        try {
            motocicletaService.save(motocicleta)
        } catch (ValidationException e) {
            respond motocicleta.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'motocicleta.label', default: 'Motocicleta'), motocicleta.id])
                redirect motocicleta
            }
            '*'{ respond motocicleta, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        motocicletaService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'motocicleta.label', default: 'Motocicleta'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'motocicleta.label', default: 'Motocicleta'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
