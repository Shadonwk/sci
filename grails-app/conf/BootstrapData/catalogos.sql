
insert into usuario (id, version, username, nombre, a_paterno, a_materno, date_created, last_updated, usuario_modifica_id, password, account_expired, account_locked, password_expired, enabled) values (1, 0, 'admin', 'ADMINISTRADOR', 'DEL', 'SISTEMA', current_timestamp,  current_timestamp ,1, 'c70cee7fb85a185c90f4e1092a2fb5522bca6837463aa7bb733cf907ea5d2f62', false, false, false, true);

insert into rol(id, version, authority, date_created, last_updated, usuario_modifica_id) values (1, 0, 'ROLE_ADMIN', current_timestamp, current_timestamp,1);
insert into rol(id, version, authority, date_created, last_updated, usuario_modifica_id) values (2, 0, 'ROLE_USER', current_timestamp, current_timestamp,1);

insert into usuario_rol(usuario_id, rol_id) values (1,1);